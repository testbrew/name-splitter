package com.testbrew.groovy

import com.testbrew.groovy.impl.NameBeginWithNumberParser
import com.testbrew.groovy.impl.NoParser
import com.testbrew.groovy.impl.SimpleNameParser
import com.testbrew.groovy.model.ParserInterface

class NameParser {

    static void main(String[] args) {
        System.out.println("Parsing names with Groovy is fun!")
        // Get the userName from system property
        String userName = System.getProperty("user.name")
        if (!userName)
            printUsage()
        // Start the parsing process
        ParserInterface parser = nameParser(userName)
        println parser.execute()
    }

    // TODO: Make more dynamic
    def static nameParser(String userName) {
        ParserInterface parser
        checkLegalUserName(userName)
        if ((userName =~ /^([0-9])+/).find()) {
            parser = new NameBeginWithNumberParser(userName)
        } else if ((userName =~ /([A-Za-z0-9\. \-_])\s+([0-9])/).find()) {
            parser = new SimpleNameParser(userName)
        } else
            parser = new NoParser(userName)
        return parser
    }

    private static void checkLegalUserName(String userName) {
        if (userName?.split()?.length <= 1)
            new NoParser(userName);
    }

    static void printUsage() {
        println """
                Usage: ./gradlew clean run -Duser.name=:username_input

                Example: ./gradlew clean run -Duser.name='Mock 3'"""
        System.exit(1)
    }
}

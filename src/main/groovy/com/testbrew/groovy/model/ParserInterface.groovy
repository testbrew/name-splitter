package com.testbrew.groovy.model


interface ParserInterface {

    abstract def execute()
}

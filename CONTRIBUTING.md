Roy Grossman [Twitter] | [LinkedIn] | [StackoverFlow] | [GitHub] | [Testbrew.com]

[twitter]: https://twitter.com/roygrssmn
[linkedin]: https://www.linkedin.com/in/roy-g/
[stackoverFlow]: https://stackoverflow.com/users/3719024/royg
[github]: https://github.com/roygrssmn
[testbrew.com]: https://testbrew.com
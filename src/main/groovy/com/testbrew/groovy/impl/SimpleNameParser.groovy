package com.testbrew.groovy.impl

import com.testbrew.groovy.model.ParserInterface

class SimpleNameParser extends ParserImpl implements ParserInterface {

    def nameRegex
    def ageRegex

    SimpleNameParser(args) {
        this.inputName = args
        setRegex()
    }

    private ParserInterface setRegex() {
        this.nameRegex = /(?<name>[A-Za-zäöüß\.\- ]+)\s?/
        this.ageRegex = /(?<age>\d+\s?[A-Za-z-\/._: ]?\d*)/
        this
    }

    def execute() {
        ArrayList<String> result = []
        return parseByRegex(result)
    }
}

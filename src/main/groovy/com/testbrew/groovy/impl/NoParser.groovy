package com.testbrew.groovy.impl

import com.testbrew.groovy.model.ParserInterface

class NoParser extends ParserImpl implements ParserInterface {

    // C'tor
    NoParser(args) {
        this.inputName = args
    }

    def execute() {
        logger.error("No parser of this type was found {}", inputName)
        throw new NoParserMatchedException("""No regex matched the input user""")
    }

    class NoParserMatchedException extends Exception {
        NoParserMatchedException(String msg) {
            super(msg)
        }
    }

}

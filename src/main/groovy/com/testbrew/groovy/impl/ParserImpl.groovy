package com.testbrew.groovy.impl

import ch.qos.logback.classic.Logger
import org.slf4j.LoggerFactory

import java.util.regex.Matcher
import java.util.regex.Pattern

class ParserImpl {


    // Groovy gives automatic setters and getters
    def inputName

    Logger logger = LoggerFactory.getLogger(this.getClass().getSimpleName())

    /**
     * Basic parser the input using supplied regex, return group name
     * @param regex
     * @param parseInput
     * @param groupName
     * @return parsed result
     */
    def parse(String regex, String parseInput, def groupName) {
        String result
        groupName = validateGroup(regex, groupName)
        Matcher matcher = Pattern.compile(regex).matcher(parseInput)
        if (matcher.find())
            result = matcher.group(groupName)

        return result?.trim()
    }

    /**
     * Validate group matches the regex
     * @param regex
     * @param groupName
     * @return the group name
     */
    private String validateGroup(String regex, def groupName) {
        if (regex.contains("?<$groupName>")) {
            logger.debug("Group '{}' found in regex: '{}'", groupName, regex)
        } else {
            logger.error("Group '{}' was not found in regex: '{}', setting regex group to 0", groupName, regex)
            groupName = 0
        }
        return groupName
    }

    /**
     * Parse the name group, regex must contain the group name
     * @param regex
     * @param parseInput
     * @return parsed result
     */
    def parseByNameGroup(String regex, String parseInput) {
        String groupName = "name"
        parse(regex, parseInput, groupName)
    }

    /**
     * Parse the age group, regex must contain the group name
     * @param regex
     * @param parseInput
     * @return parsed result
     */
    def parseByAgeGroup(String regex, String parseInput) {
        String groupName = "age"
        parse(regex, parseInput, groupName)
    }

    /**
     * Method is called from impl, is assumes that nameRegex and ageRegex is defined
     * @param result
     * @return
     */
    ArrayList<String> parseByRegex(ArrayList<String> result) {
        String userName = parseByNameGroup(nameRegex, inputName)
        String userAge = parseByAgeGroup(ageRegex, inputName)
        result.add(userName)
        result.add(userAge)
        logger.info("Input user: '{}', parsed age name: '{}', parsed name number: '{}'", inputName, userName, userAge)
        return result
    }

}

package com.testbrew.groovy.impl

import com.testbrew.groovy.model.ParserInterface


class NameBeginWithNumberParser extends ParserImpl implements ParserInterface {

    // C'tor
    NameBeginWithNumberParser(args) {
        this.inputName = args
    }

    def execute() {
        def result = []
        String userAge, userName
        (userName, userAge) = splitNameForNumberPosition()
        result.add(userName)
        result.add(userAge)
        return result
    }

    /**
     * Method check if we have more than one number in the arguments
     * If there is one block, and it is in the beginning, that is the age
     * If there is two blocks, and it is in the end, that is age
     * Method joins the remaining for the username
     * @return ArrayList of street name and number
     */
    ArrayList<String> splitNameForNumberPosition() {
        String userName, userAge
        ArrayList<String> splitUserArray = inputName.split()
        if ((splitUserArray[-1] =~ /(\d+)/).find()) {      // If the last digit is a number, treat it as age
            userAge = splitUserArray[-1]
            userName = splitUserArray[0..-2].join(" ")   // Join the array to form the username
        } else {                                           // Else the first digit is the age
            userAge = splitUserArray[0]
            userName = splitUserArray[1..-1].join(" ") // Join the array to form the username
        }
        logger.info("Input username: '{}', parsed user name: '{}', parsed age number: '{}'", inputName, userName, userAge)
        return [userName, userAge]
    }
}

import com.testbrew.groovy.NameParser
import com.testbrew.groovy.impl.NoParser.NoParserMatchedException
import com.testbrew.groovy.impl.ParserImpl
import com.testbrew.groovy.model.ParserInterface
import spock.lang.Issue
import spock.lang.Specification
import spock.lang.Unroll

class NameParserSpec extends Specification {

    @Unroll("Name parser for user: #userName")
    def "Testing name parser"() {
        when:
        ParserInterface parser = NameParser.nameParser(userName)

        and:
        def result = parser.execute()

        then:
        result == expected

        where:
        userName              | expected
        "John 3"              | ["John", "3"]
        "John 3-12"           | ["John", "3-12"]
        "John Doe 23"         | ["John Doe", "23"]
        "John Dow jr. 23"     | ["John Dow jr.", "23"]
        "23 John Dow jr."     | ["John Dow jr.", "23"]
        "John Dow-Foo-Bar 23" | ["John Dow-Foo-Bar", "23"]
    }


    @Unroll("Bad flow parser for user: #userName")
    @Issue("This user names are not yet supported")
    def "Testing bad flow userName parser"() {
        when:
        ParserInterface parser = NameParser.nameParser(userName)

        and:
        def result = parser.execute()

        then:
        result != expected

        where:
        userName            | expected
        "23 23 John Dow jr" | ["John Dow jr", "23 23"]
        "foo 3 3 aa"        | ["Foo", "3 3 aa"]
    }


    @Unroll("Bad flow - Check no parser for user: #userName")
    def "Testing bad flow no user parser"() {
        when:
        ParserInterface parser = NameParser.nameParser(userName)

        and:
        def result = parser.execute()

        then:
        thrown NoParserMatchedException

        and:
        !result

        where:
        userName   | _
        "@%"       | _
        "Roy"      | _
        ""         | _
        null       | _
        "foo @bar" | _
    }

    def "Check regex groups validation"() {
        when:
        ParserImpl parser = new ParserImpl()

        and:
        def result = parser.parse(/[A-Za-z ]+\s?\d+/, "Acme 8", "number")

        then:
        thrown IllegalArgumentException

        and:
        !result
    }

}
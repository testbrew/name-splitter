## User name parser
![Pipeline] [![License]][license_link]

### Main Goal
Example for using Groovy and Spock writing an application with OOD.     
Analyze creative skills in identifying test cases.

### Description
A small application that parses a user name and returns parsed user name and age.   
* Input: string of user name  
* Output: string of user and string of age

### Technology stack
I've used OpenJDK [Java 8] and [Groovy], for testing [Spock] and [Gradle] for build tool.   
Also added [Jacoco] integration so you can check the code coverage under the `build/jacoco/` artifacts.   
For CI I've used [GitLab].    
You can install all three using [sdkman]
> Note: currently there is a Groovy bug, can run on Java 8.
 
### How to build
Build the application before you run it, this will also run the tests.
```bash
$ ./gradlew clean build
```
> Skipping the tests by addind `-x test` or removing the test task

### How to run locally
Running the application is by running the `run` task and passing the gradle parameters.   
Clone project
```bash
$ cd [project_folder]
$ ./gradlew clean run test -Duser.name="Acme Road Runner 42"
```
Don't forget to run `clean` if you would like to clear data before rerunning test locally.
```bash
./gradlew clean 
```

##### Test Output
- Test output: `build/reports`
- Test coverage: `build/jacoco/`
- JUnit xml report: `build/test-results`

##### Coverage HTML report
To generate Jacoco HTML report run `./gradlew jacocoTestReport`. Reports will be located at `build/jacoco/` 

### How to run from IDE
 * This will focus on IntelliJ but most IDE's are familiar
 * Click on `File` --> `Open` --> Navigate to the application directory --> Click on `build.gradle`
 * Import this as a project
 * Run class: Right click the `NameParserSpec` or the `NameParser` class found under src/main, and run the program. Remember to provide arguments.
 * Run as Gradle: `Run` --> `Edit configurations` --> Create a gradle run --> Add the wanted tasks --> Add the user names in the Arguments.
 
### How to run as Continues Integration
The tests run on each push, for more insight see [gitlab-ci.yml] file.

#### LICENSE
This project is licensed under Apache-2.0 License. You are free to use this project and all of it's files.
   
Feel free to give me a star if you find this project educational and beneficial. [Contact me] so I can create more content.

[Java 8]: https://docs.oracle.com/javase/8/docs/technotes/guides/install/install_overview.html
[Groovy]: https://groovy-lang.org/
[gradle]: https://gradle.org/
[Spock]: http://spockframework.org/
[jacoco]: https://www.eclemma.org/jacoco/
[sdkman]: http://sdkman.io/install.html
[gitlab]: https://gitlab.com/testbrew/name-splitter
[pipeline]: https://gitlab.com/testbrew/name-splitter/badges/master/pipeline.svg
[license]: https://img.shields.io/badge/License-Apache%202.0-blue.svg
[license_link]: https://opensource.org/licenses/Apache-2.0
[gitlab-ci.yml]: .gitlab-ci.yml
[contact me]: CONTRIBUTING.md